from flask import Flask
from flask_cors import CORS

from mongodb import get_posts_from_db
from spider import download_posts_from_wykop

app = Flask(__name__)
CORS(app)


@app.route("/")
def index():
    return "Hello World! <br/>" \
           "You are trying to reach backend of the EDWI project <br/>" \
           "Please, use the frontend site which is designed for you: <b>http://localhost:3000</b>"


@app.route("/getPosts")
def get_posts():
    download_posts_from_wykop()
    return get_posts_from_db()


if __name__ == "__main__":
    app.run()
