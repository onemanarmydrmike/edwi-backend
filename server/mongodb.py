import json

import pymongo

DB_NAME = "edwi"
POSTS_COLLECTION_NAME = "posts"


def get_db():
    mongo_client = pymongo.MongoClient("mongodb://localhost:27017/")
    return mongo_client[DB_NAME]


def get_posts_collection():
    return get_db()[POSTS_COLLECTION_NAME]


def get_posts_from_db():
    print("retrieving posts from MongoDB...")

    cursor = get_posts_collection().find()
    posts = list(cursor)

    print("Posts retrieved successfully")
    return json.dumps(posts)


def insert_into_db(posts):
    posts_collection = get_posts_collection()
    for post in posts:
        post_id = post.get("id")
        count = posts_collection.find({"_id": post_id}).count()
        already_in_db = count > 0
        if not already_in_db:
            post_db = map_post_to_db_model(post)
            posts_collection.insert_one(post_db)


def map_post_to_db_model(json_model):
    db_model = json_model
    db_model["_id"] = json_model.get("id")
    del db_model["id"]
    return db_model
