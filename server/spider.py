import json
import requests

from mongodb import insert_into_db

APP_KEY = "RuMwyB0mrx"
URL = "https://a2.wykop.pl/Entries/Stream/page/1/appkey/{}".format(APP_KEY)  # mikroblok -> wszystkie wpisy


def download_posts_from_wykop():
    content = requests.get(URL).content
    data = json.loads(content)["data"]
    posts = []
    for post in data:
        posts.append({
            "id": post["id"],
            "author": post["author"]["login"],
            "avatar": post["author"]["avatar"],
            "body": post.get("body", "default"),
            "date": post["date"]

        })
    insert_into_db(posts)
